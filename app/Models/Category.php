<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = "categories";

    protected $fillable = ['name', 'slug', 'parent_id', 'type', 'image', 'active'];

    public $translatable = ['name', 'slug'];

    public function childs(){
        return $this->hasMany('App\Models\Category','parent_id', 'id');
    }

//    /**
//     * The users that belong to the product.
//     */
//    public function products()
//    {
//        return $this->belongsToMany('App\Models\Product');
//    }
}

<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['lang']], function () {
    // Rutas Genericas

    Route::get('/lang/{lang}', function ($lang) {
        session(['lang' => $lang]);
        return $lang;
    })->where([
        'lang' => 'en|es'
    ]);
    Route::get('/lang', function () {
        return config('app.locale');
    });
});

Route::group(['middleware' => ['auth:api', 'lang']], function(){
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    //Rutas para Administrador

    Route::resource('/categories', CategoryController::class);

});
